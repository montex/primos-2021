package br.unicamp.ic.extensao.inf335.primos;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

public class PrimeGenerator {
	
	public static void main(String[] args) {
		
    	// Defining possible options (using JOptSimple syntax)
    	// A letter means that options is possible, a colon (:)
    	// means it accepts a parameter
    	OptionParser parser = new OptionParser("pn:k:");
    	
    	// Parse the options from command line arguments of the program
    	OptionSet options = parser.parse(args);
    	
    	int quantos = 5;    	
    	int start = 0;
    	// If the -n option is specified use its value
    	if(options.has("n")) {
    		quantos = Integer.parseInt(options.valueOf("n").toString());
    	}
    	if(options.has("k")) {
    		start = Integer.parseInt(options.valueOf("k").toString());
    	}
    	
		if(options.has("p")) {
			System.out.println("Os primeiros " + quantos + " números primos são:");
			
			int[] primos = calculaPrimos(quantos, start);
			for(int i : primos) {
				System.out.println(i);
			}
		}else {
			System.out.println("Os " + quantos + " numeros foram gerados mas não serão imprimidos!");
		}
	}
	
	public static int[] calculaPrimos(int k, int startFrom) {
	
		int[] numeros = new int[k];

		int starti = 0;
		
		if(startFrom <= 4) {
			numeros[0] = 2;
			numeros[1] = 3;
			starti = 2;
			startFrom = 3;
		}else {
			startFrom = startFrom - 2;
			if(startFrom % 2 == 0) {
				startFrom = startFrom + 1;
			}
		}
		
		for(int i=starti; i<numeros.length; i++) {

			int candidato = startFrom;
			boolean prime = false;
			do {
				candidato = candidato + 2;
				prime = verificaPrimo(candidato);
			}while(!prime);

			numeros[i] = candidato;
			startFrom = candidato;
		}
		
		return numeros;		
	}
	
	public static boolean verificaPrimo(int candidato) {
		boolean prime = true;
		for(int d=2; d<candidato && prime; d++) {
			if(candidato % d == 0) {
				prime = false;
			}
		}
		return prime;
	}
}
