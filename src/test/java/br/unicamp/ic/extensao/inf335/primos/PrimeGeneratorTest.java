package br.unicamp.ic.extensao.inf335.primos;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PrimeGeneratorTest {

	@Test
	public void testSize() {
		int[] primos = PrimeGenerator.calculaPrimos(10,0);
		assertEquals(10, primos.length);
	}
	
	@Test
	public void testPrimeiro5() {
		int[] primosCorretos = new int[] { 2, 3, 5, 7, 11 }; 
		
		int[] primos = PrimeGenerator.calculaPrimos(5,0);
		assertArrayEquals(primosCorretos, primos);
	}
	
	@Test
	public void testPrimeiro5From5() {
		int[] primosCorretos = new int[] { 5, 7, 11, 13, 17 }; 
		
		int[] primos = PrimeGenerator.calculaPrimos(5,5);
		assertArrayEquals(primosCorretos, primos);
	}
	
}
